## Important

After cloning, please use this script to rename the project to your own case:

`https://www.npmjs.com/package/react-native-rename`

> If you want to change the root component as well, you can use the feature
> search and replace in VSCode to replace all the name "reactronTemplate" to the
> new name you want.

## Run the app for development

Fill in here you own project preferences

## Run the app for production

Fill in here you own project preferences

## Project structure and guidelines

#### Folder structure

- `store`: Each file contains the action constants, actions, reducer, selectors
  and they have to be separated by comments (See the example `dummyState.js`)
- `services`: Here we store the api handling and any thing that is using the OS
  api or browser api
- `screens`: Any thing that is registered to `react-navigation` as a screen will
  be placed in a folder under this, no matter how little functionality it is.
  Each screen folder can have a `sub_component` folder to store its own
  components. When the component can be used somewhere else, move it to
  `components`. Avoid cross importing!

  \*\* Naming convention for screen: `<FEATURE_NAME><INDENTIFIER>Screen`. For
  example: `UserRegistrationScreen`

#### Convention

- Use `axios` to handle api requests

- Use primitive value for props and move as much logic as possible outside
  components and screens.
  (https://www.techynovice.com/when-prop-drilling-slows-down-my-react-apps/). It
  is encouraged to have many components connected to redux than having the
  screen pass down props.

- Only import assets, images in the `config` folder

- Colors are defined in `config/colors.js` file and avoid using direct color
  name as export. i.e avoid `export const red = #something'` but something like
  `export const primaryColor = 'something'`

  Reference: https://thoughtbot.com/blog/naming-colors

- Styling convention: `margin` and `padding` should be 1 direction

  Reference:
  https://csswizardry.com/2012/06/single-direction-margin-declarations/

- Avoid `loading` and `error` handling in redux. If needed, use axios
  interceptor or middleware as
  the last resort.

- Try to normalize redux store. You can use `lodash.keyBy` or `normalizr` to
  normalize your api data before saving them to redux.

- Avoid writing your own Form, use `formik`

#### How tos

1. JWT Token handling

- https://www.techynovice.com/setting-up-JWT-token-refresh-mechanism-with-axios/

2. Smooth animation and gesture:

```bash
yarn add react-native-reanimated
yarn add react-native-gesture-handler

```

3. Handling modals

- Singleton Modal design: https://www.techynovice.com/manage-react-modal-with-singleton-component-design/

#### Testing

We use `react-native-testing-library` to handle testing

## Scripts

#### `fetch-translations.js`

- You need to get a `credential.json` from google API to the `scripts` folder:
  https://developers.google.com/sheets/api/quickstart/nodejs. Then, update the
  SHEET_ID according to the sheet id of your translations. Now you can run `node fetch-translation.js` to get the translation files

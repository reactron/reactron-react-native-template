/* eslint-disable no-control-regex */
/* eslint-disable no-param-reassign */
/* eslint-disable no-unused-expressions */
/* eslint-disable consistent-return */
/* eslint-disable no-shadow */
/* eslint-disable camelcase */
/* eslint-disable import/no-extraneous-dependencies */
// Sheet id of the translation sheet from google docs
const SHEET_ID = 'TODO: Input the localization sheet here';
const FOLDER_PATH = '';
const TOKEN_PATH = `${FOLDER_PATH}token.json`;
const RANGE_EACH_SHEET = 'B2:E';
const SHEET_TITLES = ['Translation'];
const SHEET_RANGES = SHEET_TITLES.map(title => `${title}!${RANGE_EACH_SHEET}`);
const SCOPES = ['https://www.googleapis.com/auth/spreadsheets.readonly'];
const LANGUAGE_CODES = ['en'];
const fs = require('fs');
const { set } = require('lodash');
const jsonfile = require('jsonfile');
const readline = require('readline');
const { google } = require('googleapis');

main();

function main() {
  authorize()
    .then(getTranslationData)
    .catch(err => console.error(err));
}

/**
 * Create an OAuth2 client with the given credentials, and then execute the
 * given callback function.
 * @param {Object} credentials The authorization client credentials.
 */
function authorize() {
  return new Promise(function createAuthorizePromise(resolve, reject) {
    jsonfile.readFile(`${FOLDER_PATH}credentials.json`, (err, { installed: credentials }) => {
      if (err) {
        return console.error('Error loading client secret file:', err);
      }
      // Authorize a client with credentials, then call the Google Sheets API.
      const { client_secret, client_id, redirect_uris } = credentials;
      const oAuth2Client = new google.auth.OAuth2(client_id, client_secret, redirect_uris[0]);
      // Check if we have previously stored a token.
      jsonfile.readFile(TOKEN_PATH, (err, token) => {
        if (err) {
          getNewToken(oAuth2Client, resolve, reject);
          return;
        }
        oAuth2Client.setCredentials(token);
        resolve(oAuth2Client);
      });
    });
  });
}

/**
 * Get and store new token after prompting for user authorization, and then
 * execute the given callback with the authorized OAuth2 client.
 * @param {google.auth.OAuth2} oAuth2Client The OAuth2 client to get token for.
 * @param {getEventsCallback} resolve The callback for the authorized client.
 */
function getNewToken(oAuth2Client, resolve, reject) {
  const authUrl = oAuth2Client.generateAuthUrl({
    access_type: 'offline',
    scope: SCOPES
  });
  console.log(`Authorize this app by visiting this url: ${authUrl}`);
  const rl = readline.createInterface({
    input: process.stdin,
    output: process.stdout
  });
  rl.question('Enter the code from that page here: ', code => {
    rl.close();
    oAuth2Client.getToken(code, (err, token) => {
      if (err || !token) {
        return reject(err);
      }
      oAuth2Client.setCredentials(token);
      // Store the token to disk for later program executions
      fs.writeFile(TOKEN_PATH, JSON.stringify(token), err => {
        if (err) {
          console.error(err);
        }
        console.info(`Token stored to: ${TOKEN_PATH}`);
      });
      resolve(oAuth2Client);
    });
  });
}

/**
 * Get translation strings and store them into JSON files
 * @param {google.auth.OAuth2} auth The authenticated Google OAuth client.
 */
function getTranslationData(auth) {
  const sheetsClient = google.sheets({ version: 'v4', auth });
  sheetsClient.spreadsheets.get(
    {
      spreadsheetId: SHEET_ID
    },
    err => {
      if (err) {
        return console.error(`The API returned an error: ${err}`);
      }
      sheetsClient.spreadsheets.values.batchGet(
        { spreadsheetId: SHEET_ID, ranges: SHEET_RANGES },
        (batchGetErr, response) => {
          if (batchGetErr) {
            return console.error(`The API returned an error: ${batchGetErr}`);
          }
          const currentTranslations = LANGUAGE_CODES.map(() => ({}));
          const { valueRanges } = response.data;
          SHEET_TITLES.forEach((title, index) => {
            const { values } = valueRanges[index];
            values &&
              values.forEach(([key, ...translations]) => {
                if (!key) {
                  return;
                }
                const finalEnglishTranslate = translations[0] || '';
                currentTranslations.forEach((translationSet, index) => {
                  set(
                    currentTranslations[index],
                    key,
                    removeBackspaces(translationSet[index] || finalEnglishTranslate)
                  );
                });
              });
          });
          currentTranslations.forEach((translation, index) => {
            jsonfile.writeFile(`${FOLDER_PATH}${LANGUAGE_CODES[index]}.json`, translation, { spaces: 2 }, err => {
              if (err) {
                console.error(err);
              }
              console.log(`Updated ${FOLDER_PATH}fi.json`);
            });
          });
        }
      );
    }
  );
}

function removeBackspaces(str) {
  while (str.indexOf('\b') !== -1) {
    str = str.replace(/\x08/, ''); // 0x08 is the ASCII code for \b
  }
  return str;
}

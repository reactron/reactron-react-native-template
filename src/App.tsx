import React from 'react';
import DummyNavigator from './navigator/DummyNavigator';

export default function App() {
  return <DummyNavigator />;
}

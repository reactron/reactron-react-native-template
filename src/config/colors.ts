const orange = 'orange';
const blue = 'aquamarine';

export const primaryColor = orange;
export const secondaryColor = blue;
export const borderColor = '#000';
export const dullColor = 'grey';

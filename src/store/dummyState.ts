import produce from 'immer';
import { createSelector } from 'reselect';

/**
 * ACTIONS
 */

export const DUMMY_ACTION = 'App/DUMMY_ACTION';

export function doSomething() {
  return { type: DUMMY_ACTION };
}

/**
 * Reducer
 */
const dummyInitialState = {};

export function dummyReducer(state = dummyInitialState, action = {}) {
  return produce(state, draftState => {
    switch (action.type) {
      case DUMMY_ACTION:
        draftState.property = 'new value';
        break;

      default:
        break;
    }
  });
}

/**
 * SELECTORS
 */

export const dummySelector = createSelector(
  [state => state],
  state => {
    return state;
  },
);

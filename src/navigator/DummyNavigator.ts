import { createStackNavigator, createAppContainer } from 'react-navigation';
import DummyScreen from '../screens/DummyScreen/DummyScreen';

export default createAppContainer(
  createStackNavigator({
    Dummy: DummyScreen,
  }),
);
